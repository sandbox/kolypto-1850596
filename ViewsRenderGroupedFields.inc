<?php
/** Helper class to render fields grouped in a View's TPL
 */
class views_gfields_ViewsRenderGroupedFields {
    /** Construct the renderer on an array of fields
     * @param $fields
     */
    function __construct($fields){
        $this->fields = $fields;
    }

    /** Fields that were rendered
     * @var string[]
     */
    protected $displayed_fields = array();

    /** Render fields using grouping array
     * @param $grouping
     * @return string
     */
    function render($grouping){
        $ret = '';

        # Render fields
        foreach ($grouping as $wrap => $field_names)
            if (is_array($field_names)){ # wrapped list
                if (!empty($field_names))
                    $ret .= '<div class="'.$wrap.'">'.$this->render($field_names).'</div>';
            } else { # field '$field_names'. (int)$wrap
                $ret .= $this->render_field($field_names);
            }
        return $ret;
    }

    /** Render other fields that you've forgot to mention
     * @return string
     */
    function render_forgot(){
        $forgot_fields = array_diff(array_keys($this->fields), $this->displayed_fields);

        $ret = '';
        foreach ($forgot_fields as $field_name){
            $ret .= '<div class="i-forgot-this-field" data-field-name="'.$field_name.'">';
            $ret .= $this->render_field($field_name);
            $ret .= '</div>';
        }
        return $ret;
    }

    /** Render a single field
     * @param $field_name
     * @return string
     */
    function render_field($field_name){
        $this->displayed_fields[] = $field_name;

        if (empty($this->fields[$field_name]))
            return '';
        $field = $this->fields[$field_name];

        $ret = '';
        if (!empty($field->separator))
            $ret .= $field->separator;
        $ret .= $field->wrapper_prefix . $field->label_html . $field->content . $field->wrapper_suffix;
        return $ret;
    }
}
