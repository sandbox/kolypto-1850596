<?php

class views_plugin_row_gfields extends views_plugin_row_fields {
    function option_definition() {
        $options = parent::option_definition();
        $options['field_groups'] = array();
        $options['field_groups_arr'] = array();
        return $options;
    }

    function options_form(&$form, &$form_state) {
        parent::options_form($form, $form_state);
        hide($form['inline']);
        hide($form['separator']);
        $form['field_groups'] = array(
            '#type' => 'textarea',
            '#title' => t('Field groups definition: <code>group1/group2: field1, field2\\n</code>'),
            '#default_value' => $this->options['field_groups'],
            '#required' => true,
            '#description' => t(
                "<p>Define the field groups in the following format: <br />\r\n".
                "<p><pre>".
                    "group-first/group-first-up: field_a, field_b\r\n".
                    "group-first/group-first-down: field_c, field_d\r\n".
                "</pre>\r\n".
                "<p>This example will produce HTML like this:\r\n<br />".
                "<p><pre>".
                    "&lt;div class='group-first'&gt;\r\n".
                        "\t&lt;div class='group-first-up'&gt;(field_a)(field_b)&lt;/div&gt;\r\n".
                        "\t&lt;div class='group-first-down'&gt;(field_c)(field_d)&lt;/div&gt;\r\n".
                    "&lt;/div&gt;\r\n".
                "</pre>".
                "<p>You can have any nesting level.".
                "<p>Fields not listed in the above config will get appended to the .".
                    "<pre>".var_export($this->options['field_groups_arr'],1)
            ),
        );
    }

    /** Parse the field groups config into array
     * @param string $groups
     */
    protected function _parse_field_groups_config($field_groups){
        $field_groups_array = array();
        # Explode lines
        foreach (array_filter(array_map('trim', explode("\n", $field_groups))) as $field_group){
            # Explode <groups>:<fields>
            list($groups, $fields) = array_filter(array_map('trim', explode(':', $field_group, 2))) + array(1 => '');
            # Explode <group>/<group>
            $groups = array_filter(array_map('trim', explode('/', $groups))) + array(0 => '');
            # Explode <field>,<field>
            $fields = array_filter(array_map('trim', explode(',', $fields)));
            # Create the multi-dimensional array
            $cur = &$field_groups_array;
            foreach ($groups as $group)
                $cur = &$cur[$group]; # go deeper into the array
            $cur = $fields;
        }
        return $field_groups_array;
    }

    function options_submit(&$form, &$form_state) {
        # Preprocess the text config into an array
        $form_state['values']['row_options']['field_groups_arr'] = $this->_parse_field_groups_config($form_state['values']['row_options']['field_groups']);
        # Store the configuration
        parent::options_submit($form, $form_state);
    }


}
