<?php

/**
 * Implementa hook_views_plugins()
 */
function views_gfields_views_plugins() {
    $row_plugins['views_gfields_gfields_row'] =  array(
        # Common
        'title' => t('Grouped Fields'),
        'help' => t('Allows to wrap multiple fields in tags with custom classes.'),
        'handler' => 'views_plugin_row_gfields',
        'uses options' => TRUE,
        'type' => 'normal',
        # Style plugins
        'theme' => 'views_view_gfields',
        'uses fields' => TRUE,
        );
    return array(
        'row' => $row_plugins,
    );
}
